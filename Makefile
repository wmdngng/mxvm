
target := tdemo.exe

OBJDIR := ./obj
SRCDIR := ./src

all : $(OBJDIR) $(target)
.PHONY : clean all

CC	:= gcc
LD  := gcc
RM  := rm -f -r

CFLAGS :=  -g -MD -c
#LFLAGS := -Wl,--subsystem,windows -lws2_32 -L./iup/lib -liup -liupcd -lgdi32 -lcomdlg32 -lcomctl32  -lwinmm libpng14-14.dll
LFLAGS := -luv -lwsock32 -lws2_32 -lpsapi -liphlpapi -pthread 

srcs :=  $(wildcard $(SRCDIR)/*.c)
objs :=  $(addprefix $(OBJDIR)/, $(notdir $(srcs:.c=.o)))
deps :=  $(objs:.o=.d)
#ress :=  $(OBJDIR)/wxwx.res

-include $(deps)

$(objs) : $(OBJDIR)/%.o : $(SRCDIR)/%.c
	$(CC) -Wall -c $(CFLAGS) $< -o $@

#$(ress) : $(SRCDIR)/wxwx.rc  $(SRCDIR)/mapp.ico
#	windres.exe -i $< -O coff -o $@

$(OBJDIR) :
	gmkdir $(OBJDIR)

$(target) : $(objs) #$(ress)
	$(LD) -o $@  $^  $(LFLAGS)



clean :
	$(RM) $(ress)
	$(RM) $(target)
	$(RM) $(objs)
	$(RM) $(deps)
	grmdir $(OBJDIR)
	
#$(warning $(srcs))
#$(warning $(objs))
#$(warning $(CC))