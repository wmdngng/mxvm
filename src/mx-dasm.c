
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <pthread.h>

#include <windows.h>


void  odprintf( const char *format, ... )
{
    char buf[4096], *p = buf;
    va_list args;
    
    va_start( args, format);
    p += _vsnprintf(p, sizeof(buf) - 1, format, args );
    va_end( args );
    
    OutputDebugString(buf);
} 




typedef enum _tag_dasm_fmt
{
    DASM_T_UN = 0,
    DASM_T_A,
    DASM_T_B,
    DASM_T_C,
    DASM_T_D,
    DASM_T_E,
    
} dasm_fmt_e;


typedef struct _tag_dasm_info
{
    char * name;
    dasm_fmt_e  type;
    int  arg;
    
} dasm_info_t;


dasm_info_t  tinfo[] = {
    { "TRAP   ",         0,  0		},
    { "FCMP   ",  DASM_T_A,  0      },
    { "FUN    ",  DASM_T_A,  0      },
    { "FEQL   ",  DASM_T_A,  0      },
    { "FADD   ",  DASM_T_A,  0      },
    { "FIX    ",  DASM_T_C,  0      },
    { "FSUB   ",  DASM_T_A,  0      },
    { "FIXU   ",  DASM_T_C,  0      },
    { "FLOT   ",  DASM_T_C,  0      },
    { "FLOTI  ",  DASM_T_D,  0      },
    { "FLOTU  ",  DASM_T_C,  0      },
    { "FLOTUI ",  DASM_T_D,  0      },
    { "SFLOT  ",  DASM_T_C,  0      },
    { "SFLOTI ",  DASM_T_D,  0      },
    { "SFLOTU ",  DASM_T_C,  0      },
    { "SFLOTUI",  DASM_T_D,  0      },
    { "FMUL   ",  DASM_T_A,  0      },
    { "FCMPE  ",  DASM_T_A,  0      },
    { "FUNE   ",  DASM_T_A,  0      },
    { "FEQLE  ",  DASM_T_A,  0      },
    { "FDIV   ",  DASM_T_A,  0      },
    { "FSQRT  ",  DASM_T_C,  0      },
    { "FREM   ",  DASM_T_A,  0      },
    { "FINT   ",  DASM_T_C,  0      },
    { "MUL    ",  DASM_T_A,  0      },
    { "MULI   ",  DASM_T_B,  0      },
    { "MULU   ",         0,  0      },
    { "MULUI  ",         0,  0      },
    { "DIV    ",  DASM_T_A,  0      },
    { "DIVI   ",  DASM_T_B,  0      },
    { "DIVU   ",         0,  0      },
    { "DIVUI  ",         0,  0      },
    { "ADD    ",  DASM_T_A,  0      },
    { "ADDI   ",  DASM_T_B,  0      },
    { "ADDU   ",  DASM_T_A,  0      },
    { "ADDUI  ",  DASM_T_B,  0      },
    { "SUB    ",  DASM_T_A,  0      },
    { "SUBI   ",  DASM_T_B,  0      },
    { "SUBU   ",  DASM_T_A,  0      },
    { "SUBUI  ",  DASM_T_B,  0      },
    { "2ADDU  ",  DASM_T_A,  0      },
    { "2ADDUI ",  DASM_T_B,  0      },
    { "4ADDU  ",  DASM_T_A,  0      },
    { "4ADDUI ",  DASM_T_B,  0      },
    { "8ADDU  ",  DASM_T_A,  0      },
    { "8ADDUI ",  DASM_T_B,  0      },
    { "16ADDU ",  DASM_T_A,  0      },
    { "16ADDUI",  DASM_T_B,  0      },
    { "CMP    ",  DASM_T_A,  0      },
    { "CMPI   ",  DASM_T_B,  0      },
    { "CMPU   ",  DASM_T_A,  0      },
    { "CMPUI  ",  DASM_T_B,  0      },
    { "NEG    ",         0,  0      },
    { "NEGI   ",         0,  0      },
    { "NEGU   ",         0,  0      },
    { "NEGUI  ",         0,  0      },
    { "SL     ",  DASM_T_A,  0      },
    { "SLI    ",  DASM_T_B,  0      },
    { "SLU    ",  DASM_T_A,  0      },
    { "SLUI   ",  DASM_T_B,  0      },
    { "SR     ",  DASM_T_A,  0      },
    { "SRI    ",  DASM_T_B,  0      },
    { "SRU    ",  DASM_T_A,  0      },
    { "SRUI   ",  DASM_T_B,  0      },
    { "BN     ",         0,  0      },
    { "BNB    ",         0,  0      },
    { "BZ     ",         0,  0      },
    { "BZB    ",         0,  0      },
    { "BP     ",         0,  0      },
    { "BPB    ",         0,  0      },
    { "BOD    ",         0,  0      },
    { "BODB   ",         0,  0      },
    { "BNN    ",         0,  0      },
    { "BNNB   ",         0,  0      },
    { "BNZ    ",         0,  0      },
    { "BNZB   ",         0,  0      },
    { "BNP    ",         0,  0      },
    { "BNPB   ",         0,  0      },
    { "BEV    ",         0,  0      },
    { "BEVB   ",         0,  0      },
    { "PBN    ",         0,  0      },
    { "PBNB   ",         0,  0      },
    { "PBZ    ",         0,  0      },
    { "PBZB   ",         0,  0      },
    { "PBP    ",         0,  0      },
    { "PBPB   ",         0,  0      },
    { "PBOD   ",         0,  0      },
    { "PBODB  ",         0,  0      },
    { "PBNN   ",         0,  0      },
    { "PBNNB  ",         0,  0      },
    { "PBNZ   ",         0,  0      },
    { "PBNZB  ",         0,  0      },
    { "PBNP   ",         0,  0      },
    { "PBNPB  ",         0,  0      },
    { "PBEV   ",         0,  0      },
    { "PBEVB  ",         0,  0      },
    { "CSN    ",  DASM_T_A,  0      },
    { "CSNI   ",  DASM_T_B,  0      },
    { "CSZ    ",  DASM_T_A,  0      },
    { "CSZI   ",  DASM_T_B,  0      },
    { "CSP    ",  DASM_T_A,  0      },
    { "CSPI   ",  DASM_T_B,  0      },
    { "CSOD   ",  DASM_T_A,  0      },
    { "CSODI  ",  DASM_T_B,  0      },
    { "CSNN   ",  DASM_T_A,  0      },
    { "CSNNI  ",  DASM_T_B,  0      },
    { "CSNZ   ",  DASM_T_A,  0      },
    { "CSNZI  ",  DASM_T_B,  0      },
    { "CSNP   ",  DASM_T_A,  0      },
    { "CSNPI  ",  DASM_T_B,  0      },
    { "CSEV   ",  DASM_T_A,  0      },
    { "CSEVI  ",  DASM_T_B,  0      },
    { "ZSN    ",  DASM_T_A,  0      },
    { "ZSNI   ",  DASM_T_B,  0      },
    { "ZSZ    ",  DASM_T_A,  0      },
    { "ZSZI   ",  DASM_T_B,  0      },
    { "ZSP    ",  DASM_T_A,  0      },
    { "ZSPI   ",  DASM_T_B,  0      },
    { "ZSOD   ",  DASM_T_A,  0      },
    { "ZSODI  ",  DASM_T_B,  0      },
    { "ZSNN   ",  DASM_T_A,  0      },
    { "ZSNNI  ",  DASM_T_B,  0      },
    { "ZSNZ   ",  DASM_T_A,  0      },
    { "ZSNZI  ",  DASM_T_B,  0      },
    { "ZSNP   ",  DASM_T_A,  0      },
    { "ZSNPI  ",  DASM_T_B,  0      },
    { "ZSEV   ",  DASM_T_A,  0      },
    { "ZSEVI  ",  DASM_T_B,  0      },
    { "LDB    ",  DASM_T_A,  0      },
    { "LDBI   ",  DASM_T_B,  0      },
    { "LDBU   ",  DASM_T_A,  0      },
    { "LDBUI  ",  DASM_T_B,  0      },
    { "LDW    ",  DASM_T_A,  0      },
    { "LDWI   ",  DASM_T_B,  0      },
    { "LDWU   ",  DASM_T_A,  0      },
    { "LDWUI  ",  DASM_T_B,  0      },
    { "LDT    ",  DASM_T_A,  0      },
    { "LDTI   ",  DASM_T_B,  0      },
    { "LDTU   ",  DASM_T_A,  0      },
    { "LDTUI  ",  DASM_T_B,  0      },
    { "LDO    ",  DASM_T_A,  0      },
    { "LDOI   ",  DASM_T_B,  0      },
    { "LDOU   ",  DASM_T_A,  0      },
    { "LDOUI  ",  DASM_T_B,  0      },
    { "LDSF   ",  DASM_T_A,  0      },
    { "LDSFI  ",  DASM_T_B,  0      },
    { "LDHT   ",  DASM_T_A,  0      },
    { "LDHTI  ",  DASM_T_B,  0      },
    { "CSWAP  ",  DASM_T_A,  0      },
    { "CSWAPI ",  DASM_T_B,  0      },
    { "LDUNC  ",  DASM_T_A,  0      },
    { "LDUNCI ",  DASM_T_B,  0      },
    { "LDVTS  ",         0,  0      },
    { "LDVTSI ",         0,  0      },
    { "PRELD  ",         0,  0      },
    { "PRELDI ",         0,  0      },
    { "PREGO  ",         0,  0      },
    { "PREGOI ",         0,  0      },
    { "GO     ",         0,  0      },
    { "GOI    ",         0,  0      },
    { "STB    ",  DASM_T_A,  0      },
    { "STBI   ",  DASM_T_B,  0      },
    { "STBU   ",  DASM_T_A,  0      },
    { "STBUI  ",  DASM_T_B,  0      },
    { "STW    ",  DASM_T_A,  0      },
    { "STWI   ",  DASM_T_B,  0      },
    { "STWU   ",  DASM_T_A,  0      },
    { "STWUI  ",  DASM_T_B,  0      },
    { "STT    ",  DASM_T_A,  0      },
    { "STTI   ",  DASM_T_B,  0      },
    { "STTU   ",  DASM_T_A,  0      },
    { "STTUI  ",  DASM_T_B,  0      },
    { "STO    ",  DASM_T_A,  0      },
    { "STOI   ",  DASM_T_B,  0      },
    { "STOU   ",  DASM_T_A,  0      },
    { "STOUI  ",  DASM_T_B,  0      },
    { "STSF   ",  DASM_T_A,  0      },
    { "STSFI  ",  DASM_T_B,  0      },
    { "STHT   ",  DASM_T_A,  0      },
    { "STHTI  ",  DASM_T_B,  0      },
    { "STCO   ",         0,  0      },
    { "STCOI  ",         0,  0      },
    { "STUNC  ",  DASM_T_A,  0      },
    { "STUNCI ",  DASM_T_B,  0      },
    { "SYNCD  ",         0,  0      },
    { "SYNCDI ",         0,  0      },
    { "PREST  ",         0,  0      },
    { "PRESTI ",         0,  0      },
    { "SYNCID ",         0,  0      },
    { "SYNCIDI",         0,  0      },
    { "PUSHGO ",  DASM_T_A,  0      },
    { "PUSHGOI",  DASM_T_B,  0      },
    { "OR     ",  DASM_T_A,  0      },
    { "ORI    ",  DASM_T_B,  0      },
    { "ORN    ",  DASM_T_A,  0      },
    { "ORNI   ",  DASM_T_B,  0      },
    { "NOR    ",  DASM_T_A,  0      },
    { "NORI   ",  DASM_T_B,  0      },
    { "XOR    ",  DASM_T_A,  0      },
    { "XORI   ",  DASM_T_B,  0      },
    { "AND    ",  DASM_T_A,  0      },
    { "ANDI   ",  DASM_T_B,  0      },
    { "ANDN   ",  DASM_T_A,  0      },
    { "ANDNI  ",  DASM_T_B,  0      },
    { "NAND   ",  DASM_T_A,  0      },
    { "NANDI  ",  DASM_T_B,  0      },
    { "NXOR   ",  DASM_T_A,  0      },
    { "NXORI  ",  DASM_T_B,  0      },
    { "BDIF   ",  DASM_T_A,  0      },
    { "BDIFI  ",  DASM_T_B,  0      },
    { "WDIF   ",  DASM_T_A,  0      },
    { "WDIFI  ",  DASM_T_B,  0      },
    { "TDIF   ",  DASM_T_A,  0      },
    { "TDIFI  ",  DASM_T_B,  0      },
    { "ODIF   ",  DASM_T_A,  0      },
    { "ODIFI  ",  DASM_T_B,  0      },
    { "MUX    ",  DASM_T_A,  0      },
    { "MUXI   ",  DASM_T_B,  0      },
    { "SADD   ",  DASM_T_A,  0      },
    { "SADDI  ",  DASM_T_B,  0      },
    { "MOR    ",  DASM_T_A,  0      },
    { "MORI   ",  DASM_T_B,  0      },
    { "MXOR   ",  DASM_T_A,  0      },
    { "MXORI  ",  DASM_T_B,  0      },
    { "SETH   ",  DASM_T_E,  0      },
    { "SETMH  ",  DASM_T_E,  0      },
    { "SETML  ",  DASM_T_E,  0      },
    { "SETL   ",  DASM_T_E,  0      },
    { "INCH   ",  DASM_T_E,  0      },
    { "INCMH  ",  DASM_T_E,  0      },
    { "INCML  ",  DASM_T_E,  0      },
    { "INCL   ",  DASM_T_E,  0      },
    { "ORH    ",  DASM_T_E,  0      },
    { "ORMH   ",  DASM_T_E,  0      },
    { "ORML   ",  DASM_T_E,  0      },
    { "ORL    ",  DASM_T_E,  0      },
    { "ANDNH  ",  DASM_T_E,  0      },
    { "ANDNMH ",  DASM_T_E,  0      },
    { "ANDNML ",  DASM_T_E,  0      },
    { "ANDNL  ",  DASM_T_E,  0      },
    { "JMP    ",         0,  0      },
    { "JMPB   ",         0,  0      },
    { "PUSHJ  ",         0,  0      },
    { "PUSHJB ",         0,  0      },
    { "GETA   ",         0,  0      },
    { "GETAB  ",         0,  0      },
    { "PUT    ",         0,  0      },
    { "PUTI   ",         0,  0      },
    { "POP    ",         0,  0      },
    { "RESUME ",         0,  0      },
    { "SAVE   ",         0,  0      },
    { "UNSAVE ",         0,  0      },
    { "SYNC   ",         0,  0      },
    { "SWYM   ",         0,  0      },
    { "GET    ",         0,  0      },
    { "TRIP   ",         0,  0      },
};

char * tround[] = {
    "off",
    "up",
    "down",
    "near",
};

int  dasm_disp_inst( uint64_t cpc, uint32_t instr )
{
    unsigned int  op, xx, yy, zz;
    unsigned int  yz;
    char * round;
    
    /**/
    op = instr >> 24;
    xx = (instr >> 16) & 0xff;
    yy = (instr >> 8) & 0xff;
    zz = instr & 0xff;

    /**/
    odprintf( "%I64x: %08x :", cpc, instr );
    
    switch ( tinfo[op].type )
    {
    case DASM_T_A:
        odprintf( "%s    $%d, $%d, $%d\n", tinfo[op].name, xx, yy, zz );
        break;
        
    case DASM_T_B:
        odprintf( "%s    $%d, $%d, %u\n", tinfo[op].name, xx, yy, zz );
        break;

    case DASM_T_C:
        if ( yy == 0 )
        {
            odprintf( "%s    $%d, $%d\n", tinfo[op].name, xx, zz );
        }
        else if ( yy > 4 )
        {
            odprintf( "%s    $%d, RSV, $%d\n", tinfo[op].name, xx, zz );
        }
        else
        {
            round = tround[yy];
            odprintf( "%s    $%d, %s, $%d\n", tinfo[op].name, xx, round, zz );
        }
        break;

    case DASM_T_D:
        if ( yy == 0 )
        {
            odprintf( "%s    $%d, %u\n", tinfo[op].name, xx, zz );
        }
        else if ( yy > 4 )
        {
            odprintf( "%s    $%d, RSV, %u\n", tinfo[op].name, xx, zz );
        }
        else
        {
            round = tround[yy];
            odprintf( "%s    $%d, %s, %u\n", tinfo[op].name, xx, round, zz );
        }
        break;

    case DASM_T_E:
        /* SETH $X,YZ */
        yz = (yy << 8) | zz;
        odprintf( "%s    $%d, %u\n", tinfo[op].name, xx, yz );
        break;

    default:
        odprintf( "%s\n", tinfo[op].name );
        break;
        
    }

    return 0;
    
}


int  dasm_step_wait( void )
{
    Sleep(1000);
    return 0;
}


