
/* SC,SP, ring buffer */

#ifndef  __UTL_RING_H__
#define  __UTL_RING_H__


typedef struct _tag_u8_ring
{
    uintptr_t  size;
    uintptr_t  mask;
    volatile uintptr_t  head;
    volatile uintptr_t  tail;
    
    uint8_t  data[4];
    
} u8_ring_t;


#define ENQUEUE_ENTRYS() do { \
	const uintptr_t size = rg->size; \
	uintptr_t idx = head & rg->mask; \
	uintptr_t i; \
	if ( (idx + num) < size) { \
		for (i = 0; i < (num & ((~(uintptr_t)0x3))); i+=4, idx+=4) { \
			rg->data[idx] = tbl[i]; \
			rg->data[idx+1] = tbl[i+1]; \
			rg->data[idx+2] = tbl[i+2]; \
			rg->data[idx+3] = tbl[i+3]; \
		} \
		switch (num & 0x3) { \
			case 3: rg->data[idx++] = tbl[i++]; \
			case 2: rg->data[idx++] = tbl[i++]; \
			case 1: rg->data[idx++] = tbl[i++]; \
		} \
	} else { \
		for (i = 0; idx < size; i++, idx++)\
			rg->data[idx] = tbl[i]; \
		for (idx = 0; i < num; i++, idx++) \
			rg->data[idx] = tbl[i]; \
	} \
} while(0)



#define DEQUEUE_ENTRYS() do { \
    uintptr_t idx = tail & rg->mask; \
    const uintptr_t size = rg->size; \
	uintptr_t i; \
    if ((idx + num) < size) { \
        for (i = 0; i < (num & (~(uintptr_t)0x3)); i+=4, idx+=4) {\
            tbl[i] = rg->data[idx]; \
            tbl[i+1] = rg->data[idx+1]; \
            tbl[i+2] = rg->data[idx+2]; \
            tbl[i+3] = rg->data[idx+3]; \
        } \
        switch (num & 0x3) { \
            case 3: tbl[i++] = rg->data[idx++]; \
            case 2: tbl[i++] = rg->data[idx++]; \
            case 1: tbl[i++] = rg->data[idx++]; \
        } \
    } else { \
        for (i = 0; idx < size; i++, idx++) \
            tbl[i] = rg->data[idx]; \
        for (idx = 0; i < num; i++, idx++) \
            tbl[i] = rg->data[idx]; \
    } \
} while (0)



/**/
static inline int  u8_ring_entry_num( u8_ring_t * rg )
{
    return (rg->head - rg->tail);
}


static inline int  u8_ring_enqueue( u8_ring_t * rg, uint8_t * tbl, int num )
{
    int  frees;
    uintptr_t  head;

    /**/
    head = rg->head;
    frees = rg->mask + rg->tail - head;

    /**/
    if ( num > frees )
    {

        if ( frees == 0 )
        {
            return 0;
        }

        num = frees;

    }

    /**/
    ENQUEUE_ENTRYS();
    
    /**/
    __sync_synchronize ();
    rg->head = head + num;
    
    /**/
    return num;
    
}


static inline int  u8_ring_dequeue( u8_ring_t * rg, uint8_t * tbl, int num )
{
    int  entrys;
    uintptr_t  tail;

    /**/
    tail = rg->tail;
    entrys = rg->head - tail;

    /**/
    if ( num > entrys )
    {
        if ( entrys == 0 )
        {
            return 0;
        }

        num = entrys;
    }

    /**/
	DEQUEUE_ENTRYS();

	/**/
	__sync_synchronize();
    rg->tail = tail + num;
    
    return num;
    
}


int  u8_ring_init( u8_ring_t * rg, uint32_t count );

#endif

