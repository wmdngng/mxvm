
#ifndef  __MX_DASM_H__
#define  __MX_DASM_H__

int  dasm_disp_inst( uint64_t cpc, uint32_t instr );
int  dasm_step_wait( void );

#endif

