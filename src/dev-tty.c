

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <io.h>
#include <windows.h>
#include <assert.h>

#include <uv.h>

#include "mx-utils.h"
#include "utl-ring.h"
#include "mx-bus.h"
#include "dev-tty.h"



/*
0x00: ctrl, 
0x08: flag, b0 - 1 for out fifo full.
0x10: data, write for output, read for input.
*/


typedef struct _tag_dtty_context
{
    uv_tty_t  ttyout;
    uv_write_t  ttywrite;
    uv_buf_t  ttyobuf;
    uint8_t  outpad[64];
    
    uv_async_t  ntsend;

    uint32_t  oflag;

    u8_ring_t odat;
    uint8_t  rpad[64];
    
} dtty_context_t;


static int  dev_tty_for_read( intptr_t arg, uint64_t addr, uint64_t * pval )
{
    int  iret;
    dtty_context_t * pctx;

    /**/
    pctx = (dtty_context_t *)arg;

    /**/
    switch ( addr )
    {
    case 0x08:
        iret = u8_ring_entry_num( &(pctx->odat) );
        *pval = (iret >= 63);
        break;

    default:
        *pval = 0;
        break;
    }
    
    return 0;
    
}


static int  dev_tty_for_write( intptr_t arg, uint64_t addr, uint64_t val )
{
    int  iret;
    dtty_context_t * pctx;
    uint8_t  temp;

    /**/
    pctx = (dtty_context_t *)arg;

    /**/
    switch ( addr )
    {
    case 0x10:
        temp = (uint8_t)(val);
        u8_ring_enqueue( &(pctx->odat), &temp, 1 );
        iret = u8_ring_entry_num( &(pctx->odat) );
        if ( iret == 1 )
        {
            /* need barriers ? */
            uv_async_send( &(pctx->ntsend) );
        }
        break;
        
    default:
        break;
    }
    
    return 0;
    
}


static void dev_tty_write_cb( uv_write_t* req, int status )
{
    int  iret;
    dtty_context_t * pctx;

    /**/
    pctx = (dtty_context_t *)(req->handle->data);
    
    iret = u8_ring_dequeue( &(pctx->odat), (uint8_t *)(pctx->ttyobuf.base), 64 );
    if ( iret > 0 )
    {
        pctx->ttyobuf.len = iret;
        iret = uv_write( &(pctx->ttywrite), 
            (uv_stream_t *)&(pctx->ttyout),
            &(pctx->ttyobuf),
            1,
            dev_tty_write_cb );

        assert ( iret == 0 );

    }

    return;
    
}


static void dev_tty_async_cb( uv_async_t* handle ) 
{
    dtty_context_t * pctx;
    int  iret;
    
    /**/
    pctx = (dtty_context_t *)(handle->data);

    /**/
    if ( pctx->ttyout.write_reqs_pending > 0 )
    {
        return;
    }
    
    /**/
    iret = u8_ring_dequeue( &(pctx->odat), (uint8_t *)(pctx->ttyobuf.base), 64 );
    if ( iret > 0 )
    {
        pctx->ttyobuf.len = iret;
        iret = uv_write( &(pctx->ttywrite), 
            (uv_stream_t *)&(pctx->ttyout),
            &(pctx->ttyobuf),
            1,
            dev_tty_write_cb );

        assert ( iret == 0 );
    }
    
    return;
    
}


int  dev_tty_init( uv_loop_t * ploop, uint16_t bid )
{
    int  iret;
    dtty_context_t * pctx;
    
    /**/
    pctx = (dtty_context_t *)malloc( sizeof(dtty_context_t) );
    if ( NULL == pctx )
    {
        return 1;
    }

    /**/
    pctx->oflag = 0;
    u8_ring_init( &(pctx->odat), 64 );

    {
        int  ttyout_fd;
        HANDLE  handle;
      
        handle = CreateFileA("conout$", 
            GENERIC_READ | GENERIC_WRITE,
            FILE_SHARE_READ | FILE_SHARE_WRITE,
            NULL,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            NULL);
            
        if( handle == INVALID_HANDLE_VALUE)
        {
            return 2;
        }
        ttyout_fd = _open_osfhandle((intptr_t) handle, 0);
        if ( ttyout_fd <= 0 )
        {
            return 3;
        }

        iret = uv_tty_init( ploop, &(pctx->ttyout), ttyout_fd, 0 );
        if ( 0 != iret )
        {
            return 4;
        }

        /* set to raw mode */
        uv_tty_set_mode( &(pctx->ttyout), 1 );
        
        pctx->ttyout.data = (void *)pctx;
        pctx->ttyobuf.base = (void *)(pctx->outpad);
    }
    
    /**/
    iret = uv_async_init( ploop, &(pctx->ntsend), dev_tty_async_cb );
    pctx->ntsend.data = (void *)pctx;

    /**/
    iret = mbus_register_peri( bid, 1, dev_tty_for_read, dev_tty_for_write, (intptr_t)pctx );
    if ( 0 != iret )
    {
        return 3;
    }
    
    return 0;
    
}


