#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <pthread.h>

#include "mx-bus.h"
#include "mx-dasm.h"
#include "mx-arith.h"
#include "mx-core.h"


#define  RING_SIZE  (512)
#define  RING_MASK  (RING_SIZE-1)


#define  IR_BIT     (1)     // instruction tries to load from a page without read permission
#define  IW_BIT     (1<<1)  // instruction tries to store to a page without write permission;
#define  IX_BIT     (1<<2)  // instruction appears in a page without execute permission;
#define  IN_BIT     (1<<3)  // instruction refers to a negative virtual address;

#define  IK_BIT     (1<<4)  // instruction is privileged, for use by the ��kernel�� only;
#define  IB_BIT     (1<<5)  // instruction breaks the rules of MMIX
#define  IS_BIT     (1<<6)  // instruction violates security
#define  IP_BIT     (1<<7)  // instruction comes from a privileged (negative) virtual address

#define  BR_BIT     (1<<8)  // branch, jump, push, pop


typedef struct _tag_mcore_context
{
    int  idx;
    uint64_t  curpc;

    /**/
    uint64_t  rreg[RING_SIZE];
    uint64_t  greg[256];
    uint64_t  sreg[32];

    /**/
    pthread_t  pid;
    
} mcore_context_t;


static inline int  mmu_get_inst( mcore_context_t * pctx, uint64_t addr, uint32_t * pval )
{
    if ( (addr & (1ull<<63)) != 0 )
    {
        addr = addr & ~(1ull<<63);
    }
    
    return mbus_read_tetra( addr, pval );
}

static inline int  mmu_read_byte( mcore_context_t * pctx, uint64_t addr, uint8_t * pval )
{
    addr &= ~(1ull<<63);
    return mbus_read_byte( addr, pval );
}

static inline int  mmu_read_wyde( mcore_context_t * pctx, uint64_t addr, uint16_t * pval )
{
    addr &= ~(1ull<<63);
    return mbus_read_wyde( addr, pval );
}

static inline int  mmu_read_tetra( mcore_context_t * pctx, uint64_t addr, uint32_t * pval )
{
    addr &= ~(1ull<<63);
    return mbus_read_tetra( addr, pval );
}

static inline int  mmu_read_octa( mcore_context_t * pctx, uint64_t addr, uint64_t * pval )
{
    addr &= ~(1ull<<63);
    return mbus_read_octa( addr, pval );
}


static inline int  mmu_write_byte( mcore_context_t * pctx, uint64_t addr, uint8_t val )
{
    addr &= ~(1ull<<63);
    return mbus_write_byte( addr, val );
}

static inline int  mmu_write_wyde( mcore_context_t * pctx, uint64_t addr, uint16_t val )
{
    addr &= ~(1ull<<63);
    return mbus_write_wyde( addr, val );
}

static inline int  mmu_write_tetra( mcore_context_t * pctx, uint64_t addr, uint32_t val )
{
    addr &= ~(1ull<<63);
    return mbus_write_tetra( addr, val );
}

static inline int  mmu_write_octa( mcore_context_t * pctx, uint64_t addr, uint64_t val )
{
    addr &= ~(1ull<<63);
    return mbus_write_octa( addr, val );
}


typedef enum {
  TRAP = 0,
  FCMP,FUN,FEQL,FADD,FIX,FSUB,FIXU,
  FLOT,FLOTI,FLOTU,FLOTUI,SFLOT,SFLOTI,SFLOTU,SFLOTUI,
  FMUL,FCMPE,FUNE,FEQLE,FDIV,FSQRT,FREM,FINT,
  MUL,MULI,MULU,MULUI,DIV,DIVI,DIVU,DIVUI,
  ADD,ADDI,ADDU,ADDUI,SUB,SUBI,SUBU,SUBUI,
  IIADDU,IIADDUI,IVADDU,IVADDUI,VIIIADDU,VIIIADDUI,XVIADDU,XVIADDUI,
  CMP,CMPI,CMPU,CMPUI,NEG,NEGI,NEGU,NEGUI,
  SL,SLI,SLU,SLUI,SR,SRI,SRU,SRUI,
  BN,BNB,BZ,BZB,BP,BPB,BOD,BODB,
  BNN,BNNB,BNZ,BNZB,BNP,BNPB,BEV,BEVB,
  PBN,PBNB,PBZ,PBZB,PBP,PBPB,PBOD,PBODB,
  PBNN,PBNNB,PBNZ,PBNZB,PBNP,PBNPB,PBEV,PBEVB,
  CSN,CSNI,CSZ,CSZI,CSP,CSPI,CSOD,CSODI,
  CSNN,CSNNI,CSNZ,CSNZI,CSNP,CSNPI,CSEV,CSEVI,
  ZSN,ZSNI,ZSZ,ZSZI,ZSP,ZSPI,ZSOD,ZSODI,
  ZSNN,ZSNNI,ZSNZ,ZSNZI,ZSNP,ZSNPI,ZSEV,ZSEVI,
  LDB,LDBI,LDBU,LDBUI,LDW,LDWI,LDWU,LDWUI,
  LDT,LDTI,LDTU,LDTUI,LDO,LDOI,LDOU,LDOUI,
  LDSF,LDSFI,LDHT,LDHTI,CSWAP,CSWAPI,LDUNC,LDUNCI,
  LDVTS,LDVTSI,PRELD,PRELDI,PREGO,PREGOI,GO,GOI,
  STB,STBI,STBU,STBUI,STW,STWI,STWU,STWUI,
  STT,STTI,STTU,STTUI,STO,STOI,STOU,STOUI,
  STSF,STSFI,STHT,STHTI,STCO,STCOI,STUNC,STUNCI,
  SYNCD,SYNCDI,PREST,PRESTI,SYNCID,SYNCIDI,PUSHGO,PUSHGOI,
  OR,ORI,ORN,ORNI,NOR,NORI,XOR,XORI,
  AND,ANDI,ANDN,ANDNI,NAND,NANDI,NXOR,NXORI,
  BDIF,BDIFI,WDIF,WDIFI,TDIF,TDIFI,ODIF,ODIFI,
  MUX,MUXI,SADD,SADDI,MOR,MORI,MXOR,MXORI,
  SETH,SETMH,SETML,SETL,INCH,INCMH,INCML,INCL,
  ORH,ORMH,ORML,ORL,ANDNH,ANDNMH,ANDNML,ANDNL,
  JMP,JMPB,PUSHJ,PUSHJB,GETA,GETAB,PUT,PUTI,
  POP,RESUME,SAVE,UNSAVE,SYNC,SWYM,GET,TRIP
  
}mmix_opcode;





typedef enum {
  srB,srD,srE,srH,srJ,srM,srR,srBB,
  srC,srN,srO,srS,srI,srT,srTT,srK,srQ,srU,srV,srG,srL,
  srA,srF,srP,srW,srX,srY,srZ,srWW,srXX,srYY,srZZ
  
} special_reg;


#if 1
typedef enum {
    INS_T_A = 1,        /* ADD    $x, $y, $z    */
    INS_T_B,            /* ADD    $x, $y, z     */

    INS_T_C,            /* FLOT   $x, y, $z     */
    INS_T_D,            /* FLOTI  $x, y, z      */

    INS_T_E,            /* SETL   $x, yz        */

    INS_T_F,            /* BN     $X, @+4*YZ    */

    INS_T_G,            /* LDB    $X, $y, $z    */
    INS_T_H,            /* LDBI   $X, $y, z     */

    INS_T_I,            /* STB    $X, $y, $z    */
    INS_T_J,            /* STBI   $X, $y, z     */

    INS_T_K,            /* STCO   X, $y, $z    */
    INS_T_L,            /* STCOI  X, $y, z     */

    INS_T_M,            /* PUT  X,$Z */
    INS_T_N,            /* PUTI X,Z */

    INS_T_O,            /* divu */
    INS_T_P,            /* divu */
    
    INS_T_Z,            /* misc, no decode. */
    
} inst_type_e;

struct _tag_inst_info {
    int  type;
    int  arg;
} iinfo[] = {
/* TRAP   */  {       0, 0 },
/* FCMP   */  { INS_T_A, 0 },
/* FUN    */  { INS_T_A, 0 },
/* FEQL   */  { INS_T_A, 0 },
/* FADD   */  { INS_T_A, 0 },
/* FIX    */  { INS_T_C, 0 },
/* FSUB   */  { INS_T_A, 0 },
/* FIXU   */  { INS_T_C, 0 },
/* FLOT   */  { INS_T_C, 0 },
/* FLOTI  */  { INS_T_D, 0 },
/* FLOTU  */  { INS_T_C, 0 },
/* FLOTUI */  { INS_T_D, 0 },
/* SFLOT  */  { INS_T_C, 0 },
/* SFLOTI */  { INS_T_D, 0 },
/* SFLOTU */  { INS_T_C, 0 },
/* SFLOTUI*/  { INS_T_D, 0 },
/* FMUL   */  { INS_T_A, 0 },
/* FCMPE  */  { INS_T_A, 0 },
/* FUNE   */  { INS_T_A, 0 },
/* FEQLE  */  { INS_T_A, 0 },
/* FDIV   */  { INS_T_A, 0 },
/* FSQRT  */  { INS_T_C, 0 },
/* FREM   */  { INS_T_A, 0 },
/* FINT   */  { INS_T_C, 0 },

/* MUL    */  { INS_T_A, 0 },
/* MULI   */  { INS_T_B, 0 },
/* MULU   */  {       0, 0 },
/* MULUI  */  {       0, 0 },
/* DIV    */  { INS_T_A, 0 },
/* DIVI   */  { INS_T_B, 0 },
/* DIVU   */  { INS_T_O, 0 },
/* DIVUI  */  { INS_T_P, 0 },

/* ADD    */  { INS_T_A, 0 },
/* ADDI   */  { INS_T_B, 0 },
/* ADDU   */  { INS_T_A, 0 },
/* ADDUI  */  { INS_T_B, 0 },
/* SUB    */  { INS_T_A, 0 },
/* SUBI   */  { INS_T_B, 0 },
/* SUBU   */  { INS_T_A, 0 },
/* SUBUI  */  { INS_T_B, 0 },
/* 2ADDU  */  { INS_T_A, 0 },
/* 2ADDUI */  { INS_T_B, 0 },
/* 4ADDU  */  { INS_T_A, 0 },
/* 4ADDUI */  { INS_T_B, 0 },
/* 8ADDU  */  { INS_T_A, 0 },
/* 8ADDUI */  { INS_T_B, 0 },
/* 16ADDU */  { INS_T_A, 0 },
/* 16ADDUI*/  { INS_T_B, 0 },
/* CMP    */  { INS_T_A, 0 },
/* CMPI   */  { INS_T_B, 0 },
/* CMPU   */  { INS_T_A, 0 },
/* CMPUI  */  { INS_T_B, 0 },
/* NEG    */  { INS_T_C, 0 },
/* NEGI   */  { INS_T_D, 0 },
/* NEGU   */  { INS_T_C, 0 },
/* NEGUI  */  { INS_T_D, 0 },
/* SL     */  { INS_T_A, 0 },
/* SLI    */  { INS_T_B, 0 },
/* SLU    */  { INS_T_A, 0 },
/* SLUI   */  { INS_T_B, 0 },
/* SR     */  { INS_T_A, 0 },
/* SRI    */  { INS_T_B, 0 },
/* SRU    */  { INS_T_A, 0 },
/* SRUI   */  { INS_T_B, 0 },

/* BN     */  { INS_T_F, 0 },
/* BNB    */  { INS_T_F, 0 },
/* BZ     */  { INS_T_F, 0 },
/* BZB    */  { INS_T_F, 0 },
/* BP     */  { INS_T_F, 0 },
/* BPB    */  { INS_T_F, 0 },
/* BOD    */  { INS_T_F, 0 },
/* BODB   */  { INS_T_F, 0 },
/* BNN    */  { INS_T_F, 0 },
/* BNNB   */  { INS_T_F, 0 },
/* BNZ    */  { INS_T_F, 0 },
/* BNZB   */  { INS_T_F, 0 },
/* BNP    */  { INS_T_F, 0 },
/* BNPB   */  { INS_T_F, 0 },
/* BEV    */  { INS_T_F, 0 },
/* BEVB   */  { INS_T_F, 0 },
               
/* PBN    */  { INS_T_F, 0 },
/* PBNB   */  { INS_T_F, 0 },
/* PBZ    */  { INS_T_F, 0 },
/* PBZB   */  { INS_T_F, 0 },
/* PBP    */  { INS_T_F, 0 },
/* PBPB   */  { INS_T_F, 0 },
/* PBOD   */  { INS_T_F, 0 },
/* PBODB  */  { INS_T_F, 0 },
/* PBNN   */  { INS_T_F, 0 },
/* PBNNB  */  { INS_T_F, 0 },
/* PBNZ   */  { INS_T_F, 0 },
/* PBNZB  */  { INS_T_F, 0 },
/* PBNP   */  { INS_T_F, 0 },
/* PBNPB  */  { INS_T_F, 0 },
/* PBEV   */  { INS_T_F, 0 },
/* PBEVB  */  { INS_T_F, 0 },
      
/* CSN    */  { INS_T_A, 0 },
/* CSNI   */  { INS_T_B, 0 },
/* CSZ    */  { INS_T_A, 0 },
/* CSZI   */  { INS_T_B, 0 },
/* CSP    */  { INS_T_A, 0 },
/* CSPI   */  { INS_T_B, 0 },
/* CSOD   */  { INS_T_A, 0 },
/* CSODI  */  { INS_T_B, 0 },
/* CSNN   */  { INS_T_A, 0 },
/* CSNNI  */  { INS_T_B, 0 },
/* CSNZ   */  { INS_T_A, 0 },
/* CSNZI  */  { INS_T_B, 0 },
/* CSNP   */  { INS_T_A, 0 },
/* CSNPI  */  { INS_T_B, 0 },
/* CSEV   */  { INS_T_A, 0 },
/* CSEVI  */  { INS_T_B, 0 },

/* ZSN    */  { INS_T_A, 0 },
/* ZSNI   */  { INS_T_B, 0 },
/* ZSZ    */  { INS_T_A, 0 },
/* ZSZI   */  { INS_T_B, 0 },
/* ZSP    */  { INS_T_A, 0 },
/* ZSPI   */  { INS_T_B, 0 },
/* ZSOD   */  { INS_T_A, 0 },
/* ZSODI  */  { INS_T_B, 0 },
/* ZSNN   */  { INS_T_A, 0 },
/* ZSNNI  */  { INS_T_B, 0 },
/* ZSNZ   */  { INS_T_A, 0 },
/* ZSNZI  */  { INS_T_B, 0 },
/* ZSNP   */  { INS_T_A, 0 },
/* ZSNPI  */  { INS_T_B, 0 },
/* ZSEV   */  { INS_T_A, 0 },
/* ZSEVI  */  { INS_T_B, 0 },

/* LDB    */  { INS_T_G, 0 },
/* LDBI   */  { INS_T_H, 0 },
/* LDBU   */  { INS_T_G, 0 },
/* LDBUI  */  { INS_T_H, 0 },
/* LDW    */  { INS_T_G, 0 },
/* LDWI   */  { INS_T_H, 0 },
/* LDWU   */  { INS_T_G, 0 },
/* LDWUI  */  { INS_T_H, 0 },
/* LDT    */  { INS_T_G, 0 },
/* LDTI   */  { INS_T_H, 0 },
/* LDTU   */  { INS_T_G, 0 },
/* LDTUI  */  { INS_T_H, 0 },
/* LDO    */  { INS_T_G, 0 },
/* LDOI   */  { INS_T_H, 0 },
/* LDOU   */  { INS_T_G, 0 },
/* LDOUI  */  { INS_T_H, 0 },
/* LDSF   */  { INS_T_G, 0 },
/* LDSFI  */  { INS_T_H, 0 },
/* LDHT   */  { INS_T_G, 0 },
/* LDHTI  */  { INS_T_H, 0 },

/* CSWAP  */  {       0, 0 },
/* CSWAPI */  {       0, 0 },

/* LDUNC  */  { INS_T_G, 0 },
/* LDUNCI */  { INS_T_H, 0 },
/* LDVTS  */  {       0, 0 },
/* LDVTSI */  {       0, 0 },
/* PRELD  */  { INS_T_K, 0 },
/* PRELDI */  { INS_T_L, 0 },
/* PREGO  */  { INS_T_K, 0 },
/* PREGOI */  { INS_T_L, 0 },

/* GO     */  { INS_T_G, 0 },
/* GOI    */  { INS_T_H, 0 },

/* STB    */  { INS_T_I, 0 },
/* STBI   */  { INS_T_J, 0 },
/* STBU   */  { INS_T_I, 0 },
/* STBUI  */  { INS_T_J, 0 },
/* STW    */  { INS_T_I, 0 },
/* STWI   */  { INS_T_J, 0 },
/* STWU   */  { INS_T_I, 0 },
/* STWUI  */  { INS_T_J, 0 },
/* STT    */  { INS_T_I, 0 },
/* STTI   */  { INS_T_J, 0 },
/* STTU   */  { INS_T_I, 0 },
/* STTUI  */  { INS_T_J, 0 },
/* STO    */  { INS_T_I, 0 },
/* STOI   */  { INS_T_J, 0 },
/* STOU   */  { INS_T_I, 0 },
/* STOUI  */  { INS_T_J, 0 },
/* STSF   */  { INS_T_I, 0 },
/* STSFI  */  { INS_T_J, 0 },
/* STHT   */  { INS_T_I, 0 },
/* STHTI  */  { INS_T_J, 0 },

/* STCO   */  { INS_T_K, 0 },
/* STCOI  */  { INS_T_L, 0 },

/* STUNC  */  { INS_T_I, 0 },
/* STUNCI */  { INS_T_J, 0 },

/* SYNCD  */  { INS_T_K, 0 },
/* SYNCDI */  { INS_T_L, 0 },
/* PREST  */  { INS_T_K, 0 },
/* PRESTI */  { INS_T_L, 0 },
/* SYNCID */  { INS_T_K, 0 },
/* SYNCIDI*/  { INS_T_L, 0 },

/* PUSHGO */  { INS_T_G, 0 },
/* PUSHGOI*/  { INS_T_H, 0 },

/* OR     */  { INS_T_A, 0 },
/* ORI    */  { INS_T_B, 0 },
/* ORN    */  { INS_T_A, 0 },
/* ORNI   */  { INS_T_B, 0 },
/* NOR    */  { INS_T_A, 0 },
/* NORI   */  { INS_T_B, 0 },
/* XOR    */  { INS_T_A, 0 },
/* XORI   */  { INS_T_B, 0 },
/* AND    */  { INS_T_A, 0 },
/* ANDI   */  { INS_T_B, 0 },
/* ANDN   */  { INS_T_A, 0 },
/* ANDNI  */  { INS_T_B, 0 },
/* NAND   */  { INS_T_A, 0 },
/* NANDI  */  { INS_T_B, 0 },
/* NXOR   */  { INS_T_A, 0 },
/* NXORI  */  { INS_T_B, 0 },

/* BDIF   */  { INS_T_A, 0 },
/* BDIFI  */  { INS_T_B, 0 },
/* WDIF   */  { INS_T_A, 0 },
/* WDIFI  */  { INS_T_B, 0 },
/* TDIF   */  { INS_T_A, 0 },
/* TDIFI  */  { INS_T_B, 0 },
/* ODIF   */  { INS_T_A, 0 },
/* ODIFI  */  { INS_T_B, 0 },

/* MUX    */  { INS_T_A, 0 },
/* MUXI   */  { INS_T_B, 0 },

/* SADD   */  { INS_T_A, 0 },
/* SADDI  */  { INS_T_B, 0 },
/* MOR    */  { INS_T_A, 0 },
/* MORI   */  { INS_T_B, 0 },
/* MXOR   */  { INS_T_A, 0 },
/* MXORI  */  { INS_T_B, 0 },

/* SETH   */  { INS_T_E, 0 },
/* SETMH  */  { INS_T_E, 0 },
/* SETML  */  { INS_T_E, 0 },
/* SETL   */  { INS_T_E, 0 },

/* INCH   */  { INS_T_E, 0 },
/* INCMH  */  { INS_T_E, 0 },
/* INCML  */  { INS_T_E, 0 },
/* INCL   */  { INS_T_E, 0 },

/* ORH    */  { INS_T_E, 0 },
/* ORMH   */  { INS_T_E, 0 },
/* ORML   */  { INS_T_E, 0 },
/* ORL    */  { INS_T_E, 0 },

/* ANDNH  */  { INS_T_E, 0 },
/* ANDNMH */  { INS_T_E, 0 },
/* ANDNML */  { INS_T_E, 0 },
/* ANDNL  */  { INS_T_E, 0 },

/* JMP    */  { INS_T_Z, 0 },
/* JMPB   */  { INS_T_Z, 0 },
/* PUSHJ  */  { INS_T_F, 0 },
/* PUSHJB */  { INS_T_F, 0 },

/* GETA   */  { INS_T_F, 0 },
/* GETAB  */  { INS_T_F, 0 },

/* PUT    */  { INS_T_M, 0 },
/* PUTI   */  { INS_T_N, 0 },
/* POP    */  { INS_T_Z, 0 },
/* RESUME */  {       0, 0 },
/* SAVE   */  {       0, 0 },
/* UNSAVE */  {       0, 0 },
/* SYNC   */  { INS_T_Z, 0 },
/* SWYM   */  { INS_T_Z, 0 },
/* GET    */  { INS_T_Z, 0 },
/* TRIP   */  {       0, 0 },
};
#endif



static inline int mcore_register_truth( uint64_t val, uint32_t op )
{
    int  iret;

    switch ( (op>>1) & 0x3 )
    {
    case 0:
        /* negative */
        iret = ((val >> 63) == 1);
        break;

    case 1:
        /* zero */
        iret = (val == 0);
        break;

    case 2:
        /* positive */
        iret = (((val >> 63) == 0) && (val != 0));
        break;

    case 3:
        /* odd */
        iret = ((val & 0x1) != 0);
        break;
        
    }
  
    /**/
    if ( op & 0x8 )
    {
        return iret^1;
    }
    else
    {
        return iret;
    }
    
}


/* register ring */

/* store for space, and zero the regs. */
static inline int  mcore_stack_store( mcore_context_t * pctx, int num )
{
    int  i;
    uint32_t  temp;

    temp = (uint32_t)( pctx->sreg[srO] - pctx->sreg[srS] + pctx->sreg[srL] );
    if ( (temp + num) <= RING_SIZE )
    {
        return 0;
    }
    
    /**/
    temp = temp + num - RING_SIZE;
    
    for ( i=0; i<temp; i++ )
    {
        /* if stack full, need store */
        mmu_write_octa( pctx, (pctx->sreg[srS] << 3), pctx->rreg[pctx->sreg[srS] & RING_MASK] );
        pctx->rreg[pctx->sreg[srS] & RING_MASK] = 0ull;
        pctx->sreg[srS] += 1;
    }

    return 0;
    
}


/* fetech for pop. */
static inline int  mcore_stack_load( mcore_context_t * pctx, int num )
{
    int  i;
    uint32_t  temp;

    temp = (uint32_t)( pctx->sreg[srO] - pctx->sreg[srS] );
    if ( temp >= num )
    {
        return 0;
    }

    /**/
    temp = num - temp;
    for ( i=0; i<temp; i++ )
    {
        /* if stack full, need store */
        pctx->sreg[srS] -= 1;
        mmu_read_octa( pctx, (pctx->sreg[srS] << 3), &(pctx->rreg[pctx->sreg[srS] & RING_MASK]) );
    }    
    
    return 0;
    
}


static inline uint64_t  mcore_reg_fetch( mcore_context_t * pctx, uint32_t idx )
{
    uint64_t  temp = 0ull;
    
    if ( idx < pctx->sreg[srL] )
    {
        temp = pctx->rreg[ (pctx->sreg[srO] + idx) & RING_MASK  ];
    }
    else if( idx >= pctx->sreg[srG] )
    {
        temp = pctx->greg[idx];
    }
    
    return temp;
}



static inline void  mcore_reg_store( mcore_context_t * pctx, uint32_t idx, uint64_t val )
{
    uint32_t  temp;

    if ( idx < pctx->sreg[srL] )
    {
        pctx->rreg[ (pctx->sreg[srO] + idx) & RING_MASK ] = val;
    }
    else if( idx >= pctx->sreg[srG] )
    {
        pctx->greg[idx] = val;
    }
    else
    {
        temp = (uint32_t)(pctx->sreg[srL]);
        temp = idx - temp + 1;
        mcore_stack_store( pctx, (int)temp );

        /**/
        pctx->sreg[srL] += temp;
        pctx->rreg[ (pctx->sreg[srO] + idx) & RING_MASK ] = val;
    }

    return;
    
}


int  mcore_exec( mcore_context_t * pctx )
{
    int  iret;
    uint32_t  instr;
    uint32_t  op;
    uint32_t  xx, yy, zz, yz;
    uint64_t  aa, bb, cc, mm;

    uint32_t  aexec;        /* arithmatic exception, to TRIP */
    uint32_t  iexec;        /* instruction exception, to TRAP.*/
    uint64_t  dstpc;

    uint8_t   tu8;
    uint16_t   tu16;
    uint32_t   tu32;
    uint64_t   tu64;
    
rretry:
    iret = mmu_get_inst( pctx, pctx->curpc, &instr );

#if 0
    /**/
    dasm_disp_inst( pctx->curpc, instr );
    dasm_step_wait();
#endif

    /**/
    aexec = 0;
    iexec = 0;
    op = instr >> 24;
    xx = (instr >> 16) & 0xff;
    yy = (instr >> 8) & 0xff;
    zz = instr & 0xff;
    yz = instr & 0xffff;

    /**/
    switch ( iinfo[op].type )
    {
    case INS_T_A:
        bb = mcore_reg_fetch( pctx, yy );
        cc = mcore_reg_fetch( pctx, zz );
        break;
        
    case INS_T_B:
        bb = mcore_reg_fetch( pctx, yy );
        cc = zz;
        break;

    case INS_T_C:
        bb = yy;
        cc = mcore_reg_fetch( pctx, zz );
        break;

    case INS_T_D:
        bb = yy;
        cc = zz;
        break;
        
    case INS_T_E:
        bb = mcore_reg_fetch( pctx, xx );
        cc = yz;
        cc = cc << ((3 - (op & 3)) << 4);
        break;

    case INS_T_F:
        /* branch, BN  $X, @+4*YZ */
        aa = mcore_reg_fetch( pctx, xx );
        mm = pctx->curpc + (yz << 2);
        if ( op & 1 )
        {
            mm -= 0x40000;
        }
        break;

    case INS_T_G:
        bb = mcore_reg_fetch( pctx, yy );
        cc = mcore_reg_fetch( pctx, zz );
        mm = bb + cc;
        break;

    case INS_T_H:
        bb = mcore_reg_fetch( pctx, yy );
        cc = zz;
        mm = bb + cc;
        break;

    case INS_T_I:
        bb = mcore_reg_fetch( pctx, yy );
        cc = mcore_reg_fetch( pctx, zz );
        mm = bb + cc;
        aa = mcore_reg_fetch( pctx, xx );
        break;

    case INS_T_J:
        bb = mcore_reg_fetch( pctx, yy );
        cc = zz;
        mm = bb + cc;
        aa = mcore_reg_fetch( pctx, xx );
        break;

    case INS_T_K:
        bb = mcore_reg_fetch( pctx, yy );
        cc = mcore_reg_fetch( pctx, zz );
        mm = bb + cc;
        aa = xx;
        break;
        
    case INS_T_L:
        bb = mcore_reg_fetch( pctx, yy );
        cc = zz;
        mm = bb + cc;
        aa = xx;
        break;

    case INS_T_M:
        cc = mcore_reg_fetch( pctx, zz );
        break;
        
    case INS_T_N:
        cc = zz;
        break;

    case INS_T_O:
        mm = pctx->sreg[srD];
        bb = mcore_reg_fetch( pctx, yy );
        cc = mcore_reg_fetch( pctx, zz );
        break;
        
    case INS_T_P:
        mm = pctx->sreg[srD];
        bb = mcore_reg_fetch( pctx, yy );
        cc = zz;
        break;
        

    case INS_T_Z:
        break;
        
    default:
        printf( "addr: %I64x, -instr : %08x\n", pctx->curpc, instr );
        iret = 2;
        goto errexit;
    }

    /**/
    switch ( op )
    {
    case TRAP:
        break;

    case FCMP:
    case FUN:
    case FEQL:
    case FADD:
    case FIX:
    case FSUB:
    case FIXU:
    case FLOT:
    case FLOTI:
    case FLOTU:
    case FLOTUI:
    case SFLOT:
    case SFLOTI:
    case SFLOTU:
    case SFLOTUI:
    case FMUL:
    case FCMPE:
    case FUNE:
    case FEQLE:
    case FDIV:
    case FSQRT:
    case FREM:
    case FINT:
        break;
        
    case MUL:
    case MULI:
    case MULU:
    case MULUI:
        break;
        
    case DIV:
    case DIVI:
        if ( cc == 0 )
        {
            aa = 0;
            pctx->sreg[srR] = bb;
            aexec |= AD_BIT;
            mcore_reg_store( pctx, xx, aa );
            break;
        }

        if ( (bb == 0x8000000000000000ull) && (cc == 0xffffffffffffffffull ) )
        {
            aexec |= AV_BIT;
            aa = 0;
            pctx->sreg[srR] = 0;
            mcore_reg_store( pctx, xx, aa );            
            break;
        }

        aa = (uint64_t)( (int64_t)bb / (int64_t)cc );
        tu64 = (uint64_t)( (int64_t)bb % (int64_t)cc );
        pctx->sreg[srR] = tu64;
        mcore_reg_store( pctx, xx, aa );          
        break;
        
    case DIVU:
    case DIVUI:
        if ( mm >= cc )
        {
            aa = mm;
            pctx->sreg[srR] = bb;
            mcore_reg_store( pctx, xx, aa );            
            break;
        }

        if ( mm == 0 )
        {
            aa = bb / cc;
            tu64 = bb % cc;
            pctx->sreg[srR] = tu64;
            mcore_reg_store( pctx, xx, aa );            
            break;   
        }
        
        tu64 = (~cc) + 1;
        aa = (1 + (tu64 / cc)) * mm;
        tu64 = (tu64 % cc) * mm;
        aa += (tu64 / cc) + (bb / cc);
        tu64 = (tu64 % cc) + (bb % cc);

        aa += tu64 / cc;
        tu64 = tu64 % cc;

        pctx->sreg[srR] = tu64;
        mcore_reg_store( pctx, xx, aa );
        
        break;
        
    case ADD:
    case ADDI:
        aa = bb + cc;
        if ( ((bb^cc) & (1ull<<63)) == 0 )
        {
            if ( ((bb^aa) & (1ull<<63)) != 0 )
            {
                aexec |= AV_BIT;
            }
        }
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case ADDU:
    case ADDUI:
        aa = bb + cc;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case SUB:
    case SUBI:
    case NEG:
    case NEGI:
        aa = bb - cc;
        if ( ((aa^cc)&(1ull<<63))==0 )
        {
            if( ((aa^bb)&(1ull<<63))!=0 )
            {
                aexec |= AV_BIT;
            }
        }
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case SUBU:
    case SUBUI:
    case NEGU:
    case NEGUI:
        aa = bb - cc;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case IIADDU:
    case IIADDUI:
    case IVADDU:
    case IVADDUI:
    case VIIIADDU:
    case VIIIADDUI:
    case XVIADDU:
    case XVIADDUI:
        aa = bb << (((op & 0xf) >> 1) -3);
        aa = bb + cc;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case CMP:
    case CMPI:
        if ( (bb & (1ull<<63)) > (cc & (1ull<<63)) )
        {
            aa = ~0ull;
            mcore_reg_store( pctx, xx, aa );
            break;
        }

        if ( (bb & (1ull<<63)) < (cc & (1ull<<63)) )
        {
            aa = 1ull;
            mcore_reg_store( pctx, xx, aa );
            break;
        }
        
    case CMPU:
    case CMPUI:
        if ( bb > cc )
        {
            aa = 1ull;
            mcore_reg_store( pctx, xx, aa );
            break;
        }

        if ( bb < cc )
        {
            aa = ~0ull;
            mcore_reg_store( pctx, xx, aa );
            break;
        }

        /* eq */
        aa = 0;
        mcore_reg_store( pctx, xx, aa );
        break;

    case SL:
    case SLI:
        if ( cc >= 64 )
        {
            aa = 0;
            if ( bb != 0 )
            {
                aexec |= AV_BIT;
            }
        }
        else
        {
            aa = bb << cc;
            if ( __builtin_clrsbll( bb ) < cc )
            {
                aexec |= AV_BIT;
            }
        }
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case SLU:
    case SLUI:
        aa = bb << cc;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case SR:
    case SRI:
        /* todo, need test*/
        aa = (uint64_t)( (int64_t)bb >> cc );
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case SRU:
    case SRUI:
        aa = bb >> cc;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case BN:
    case BNB:
    case BZ:
    case BZB:
    case BP:
    case BPB:
    case BOD:
    case BODB:
    case BNN:
    case BNNB:
    case BNZ:
    case BNZB:
    case BNP:
    case BNPB:
    case BEV:
    case BEVB:
    case PBN:
    case PBNB:
    case PBZ:
    case PBZB:
    case PBP:
    case PBPB:
    case PBOD:
    case PBODB:
    case PBNN:
    case PBNNB:
    case PBNZ:
    case PBNZB:
    case PBNP:
    case PBNPB:
    case PBEV:
    case PBEVB:
        if ( mcore_register_truth( aa, op ) )
        {
            iexec = BR_BIT;
            dstpc = mm;
        }
        break;
    
    case CSN:
    case CSNI:
    case CSZ:
    case CSZI:
    case CSP:
    case CSPI:
    case CSOD:
    case CSODI:
    case CSNN:
    case CSNNI:
    case CSNZ:
    case CSNZI:
    case CSNP:
    case CSNPI:
    case CSEV:
    case CSEVI:
        if ( mcore_register_truth( bb, op ) )
        {
            aa = cc;
            mcore_reg_store( pctx, xx, aa );
        }
        break;
        
    case ZSN:
    case ZSNI:
    case ZSZ:
    case ZSZI:
    case ZSP:
    case ZSPI:
    case ZSOD:
    case ZSODI:
    case ZSNN:
    case ZSNNI:
    case ZSNZ:
    case ZSNZI:
    case ZSNP:
    case ZSNPI:
    case ZSEV:
    case ZSEVI:
        if ( mcore_register_truth( bb, op ) )
        {
            aa = cc;
        }
        else
        {
            aa = 0;
        }
        mcore_reg_store( pctx, xx, aa );
        break;

    case LDB:
    case LDBI:
        mmu_read_byte( pctx, mm, &tu8 );
        aa = (uint64_t)(int64_t)((int8_t)tu8);
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case LDBU:
    case LDBUI:
        mmu_read_byte( pctx, mm, &tu8 );
        aa = (uint64_t)tu8;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case LDW:
    case LDWI:
        mmu_read_wyde( pctx, mm, &tu16 );
        aa = (uint64_t)(int64_t)((int16_t)tu8);
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case LDWU:
    case LDWUI:
        mmu_read_wyde( pctx, mm, &tu16 );
        aa = (uint64_t)tu8;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case LDT:
    case LDTI:
        mmu_read_tetra( pctx, mm, &tu32 );
        aa = (uint64_t)(int64_t)((int32_t)tu32 );
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case LDTU:
    case LDTUI:
        mmu_read_tetra( pctx, mm, &tu32 );
        aa = (uint64_t)tu32;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case LDO:
    case LDOI:
    case LDOU:
    case LDOUI:
        mmu_read_octa( pctx, mm, &tu64 );
        aa = tu64;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case LDSF:
    case LDSFI:
        break;

    case LDHT:
    case LDHTI:
        mmu_read_tetra( pctx, mm, &tu32 );
        aa = (uint64_t)tu32;
        aa <<= 32;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case CSWAP:
    case CSWAPI:
        break;
        
    case LDUNC:
    case LDUNCI:
        mmu_read_octa( pctx, mm, &tu64 );
        aa = tu64;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case LDVTS:
    case LDVTSI:
    case PRELD:
    case PRELDI:
    case PREGO:
    case PREGOI:
        break;
        
    case GO:
    case GOI:
        aa = pctx->curpc + 4;
        mcore_reg_store( pctx, xx, aa );
        iexec = BR_BIT;
        dstpc = mm;
        break;
        
    case STB:
    case STBI:
        iret = __builtin_clrsbll( aa );
        if ( iret < 56 )
        {
            aexec |= AV_BIT;
        }
        mmu_write_byte( pctx, mm, (uint8_t)aa );
        break;
        
    case STBU:
    case STBUI:
        mmu_write_byte( pctx, mm, (uint8_t)aa );
        break;
        
    case STW:
    case STWI:
        iret = __builtin_clrsbll( aa );
        if ( iret < 48 )
        {
            aexec |= AV_BIT;
        }
        mmu_write_wyde( pctx, mm, (uint16_t)aa );
        break;
        
    case STWU:
    case STWUI:
        mmu_write_wyde( pctx, mm, (uint16_t)aa );
        break;
    
    case STT:
    case STTI:
        iret = __builtin_clrsbll( aa );
        if ( iret < 32 )
        {
            aexec |= AV_BIT;
        }
        mmu_write_tetra( pctx, mm, (uint32_t)aa );
        break;
        
    case STTU:
    case STTUI:
        mmu_write_tetra( pctx, mm, (uint32_t)aa );
        break;
    
    case STO:
    case STOI:
    case STOU:
    case STOUI:
        mmu_write_octa( pctx, mm, aa );
        break;
        
    case STSF:
    case STSFI:
        break;
        
    case STHT:
    case STHTI:
        mmu_write_tetra( pctx, mm, (uint32_t)(aa >> 32) );
        break;
    
    case STCO:
    case STCOI:
        mmu_write_byte( pctx, mm, (uint8_t)aa );
        break;

    case STUNC:
    case STUNCI:
        mmu_write_tetra( pctx, mm, aa );
        break;
        
    case SYNCD:
    case SYNCDI:
    case PREST:
    case PRESTI:
    case SYNCID:
    case SYNCIDI:
        break;
        
    case OR:
    case ORI:
        aa = bb | cc;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case ORN:
    case ORNI:
        aa = bb | (~cc);
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case NOR:
    case NORI:
        aa = ~(bb | cc);
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case XOR:
    case XORI:
        aa = bb ^ cc;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case AND:
    case ANDI:
        aa = bb & cc;
        mcore_reg_store( pctx, xx, aa );
        break;

    case ANDN:
    case ANDNI:
        aa = bb & (~cc);
        mcore_reg_store( pctx, xx, aa );
        break;

    case NAND:
    case NANDI:
        aa = ~(bb & cc);
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case NXOR:
    case NXORI:
        aa = ~(bb ^ cc);
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case BDIF:
    case BDIFI:
    case WDIF:
    case WDIFI:
    case TDIF:
    case TDIFI:
    case ODIF:
    case ODIFI:
    
    case MUX:
    case MUXI:
    case SADD:
    case SADDI:
    case MOR:
    case MORI:
    case MXOR:
    case MXORI:
        break;
        
    case SETH:
    case SETMH:
    case SETML:
    case SETL:
        aa = cc;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case INCH:
    case INCMH:
    case INCML:
    case INCL:
        aa = bb + cc;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case ORH:
    case ORMH:
    case ORML:
    case ORL:
        aa = bb | cc;
        mcore_reg_store( pctx, xx, aa );
        break;

    case ANDNH:
    case ANDNMH:
    case ANDNML:
    case ANDNL:
        aa = bb & cc;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case JMP:
        tu64 = instr & 0xffffff;
        iexec = BR_BIT;
        dstpc = pctx->curpc + (tu64 << 2);
        break;
        
    case JMPB:
        tu64 = instr & 0xffffff;
        tu64 = tu64 - 0x1000000ull;
        iexec = BR_BIT;
        dstpc = pctx->curpc + (tu64 << 2);
        break;

    case PUSHGO:
    case PUSHGOI:
    case PUSHJ:
    case PUSHJB:
        /**/
        if ( xx >= pctx->sreg[srG] )
        {
            xx = pctx->sreg[srL];
        }

        /**/
        if ( xx >= pctx->sreg[srL] )
        {
            if ( pctx->sreg[srL] >= pctx->sreg[srG] )
            {
                /* todo, exception */
                iret = 5;
                goto errexit;
            }

            mcore_stack_store( pctx, (int)(xx + 1) - (int)(pctx->sreg[srL]) );
            pctx->sreg[srL] = xx + 1;
        }
        
        aa = xx;
        mcore_reg_store( pctx, xx, aa );
        pctx->sreg[srJ] = pctx->curpc + 4;
       
        /* todo, need test */
        pctx->sreg[srL] -= xx + 1;
        pctx->sreg[srO] += xx + 1;
        
        /**/
        iexec = BR_BIT;
        dstpc = mm;
        break;
        
    case GETA:
    case GETAB:
        aa = mm;
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case PUT:
    case PUTI:
        if (  (yy != 0) || (xx >= 32)  )
        {
            iexec |= IB_BIT;
            break;
        }
        /* todo, push stack */
        pctx->sreg[xx] = cc;
        break;
        
    case POP:
        // xx, yz
        /**/
        mcore_stack_load( pctx, 1 );        
        tu8 = (uint8_t)( pctx->rreg[ (pctx->sreg[srO] - 1) & RING_MASK ] );
        
        /**/
        mcore_stack_load( pctx, tu8 + 1 );  
        
        /**/
        if ( xx == 0 )
        {
            if ( tu8 >= pctx->sreg[srG] )
            {
                pctx->sreg[srL] = pctx->sreg[srG];
            }
            else
            {
                pctx->sreg[srL] = tu8;
            }

            /**/
            pctx->sreg[srO] -= tu8 + 1;
            
        }
        else
        {
            /**/
            if ( xx > pctx->sreg[srL] )
            {
                tu64 = 0ull;
                tu16 = tu8 + pctx->sreg[srL] + 1;
            }
            else
            {
                tu64 = mcore_reg_fetch( pctx, xx-1 );
                tu16 = tu8 + xx;
            }
            
            /**/
            if ( tu16 >= pctx->sreg[srG] )
            {
                pctx->sreg[srL] = pctx->sreg[srG];
            }
            else
            {
                pctx->sreg[srL] = tu16;
            }

            /**/
            pctx->sreg[srO] -= tu8 + 1;
            mcore_reg_store( pctx, tu8, tu64 );
            
        }


        /**/
        iexec |= BR_BIT;
        dstpc = pctx->sreg[srJ] + (yz << 2);
        break;
        
    case RESUME:
    case SAVE:
    case UNSAVE:
        break;

    case SYNC:
    case SWYM:
        break;
        
    case GET:
        if ( (yy != 0) || (zz >= 32) )
        {
            /**/
            iexec |= IB_BIT;
            break;    
        }
        aa = pctx->sreg[zz];
        mcore_reg_store( pctx, xx, aa );
        break;
        
    case TRIP:
        break;
        
    default:
        break;
    }

    /**/
    if ( iexec == 0 )
    {
        /**/
        pctx->curpc += 4;    
    }
    else
    {
        if ( iexec & BR_BIT )
        {
            pctx->curpc = dstpc;     
        }
        else
        {
            /* todo */
            pctx->curpc += 4;
        }
    }

    /**/
    goto rretry;

errexit:
    return iret;
    
}



void *  mcore_thread( void * arg )
{
    int  iret;

    iret = mcore_exec( (mcore_context_t *)arg );
    printf( "mcore thread exit, iret = %d\n", iret );
    
    return NULL;
}


int  mcore_init( int idx )
{
    int  iret;
    mcore_context_t * pctx;

    pctx = (mcore_context_t *)malloc( sizeof(mcore_context_t) );
    if ( NULL == pctx )
    {
        return 1;
    }
    
    /**/
    memset( pctx, 0, sizeof(mcore_context_t) );
    pctx->idx = idx;

    pctx->curpc = 0x8000000000000000ull;

    /**/
    pctx->sreg[srG] = 128;
    pctx->sreg[srL] = 0;

    /* reg stack. */
    pctx->sreg[srS] = 0x100000ull;
    pctx->sreg[srO] = 0x100000ull;

    /* program stack, $254, sp;  $253, fp */
    mcore_reg_store( pctx, 254, 0x200000ull );
    mcore_reg_store( pctx, 253, 0x200000ull );


    /**/
    iret = pthread_create( &(pctx->pid), NULL, mcore_thread, (void *)pctx );
    if ( 0 != iret )
    {
        return 2;
    }
    
    return 0;
    
}

