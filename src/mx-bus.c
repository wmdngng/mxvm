

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "mx-utils.h"
#include "mx-bus.h"

#define MBUS_PERI_MAX  16

typedef struct _tag_peri_info
{
    char * name;
    uint8_t  bid;
    uint8_t  irq;
    
    intptr_t  arg;
    mbus_pread_func_t  rfunc;
    mbus_pwrite_func_t  wfunc;
    
} peri_info_t;


typedef struct _tag_mbus_context
{
    /* memory */
    size_t  memsz;
    uint8_t * pmem;
    
    /* peri */
    peri_info_t  peri_info[MBUS_PERI_MAX];
    
} mbus_context_t;

mbus_context_t g_mbus_ctx = { 0, NULL };

/**/
static int  peri_read_octa( uint8_t bid, uint64_t addr, uint64_t * pval )
{
    peri_info_t * pinfo;

    /**/
    pinfo = &(g_mbus_ctx.peri_info[bid]);
    if ( NULL == pinfo->name )
    {
        return 1;
    }

    return pinfo->rfunc( pinfo->arg, addr, pval );

}

static int  peri_write_octa( uint8_t bid, uint64_t addr, uint64_t val )
{
    peri_info_t * pinfo;

    /**/
    pinfo = &(g_mbus_ctx.peri_info[bid]);
    if ( NULL == pinfo->name )
    {
        return 1;
    }

    return pinfo->wfunc( pinfo->arg, addr, val );
}

/**/
int  mbus_read_byte( uint64_t addr, uint8_t * pval )
{
    if ( addr >= g_mbus_ctx.memsz )
    {
        return 1;
    }

    /**/
    *pval = g_mbus_ctx.pmem[addr];
    return 0;
}


int  mbus_read_wyde( uint64_t addr, uint16_t * pval )
{
    uint16_t  tval;

    addr &= ~0x1;
    if ( (addr + 1) >= g_mbus_ctx.memsz )
    {
        return 1;
    }
    
    /**/
    tval = *(uint16_t *)(g_mbus_ctx.pmem + addr);
    *pval = utls_htons(tval);
    return 0;
}


int  mbus_read_tetra( uint64_t addr, uint32_t * pval )
{
    uint32_t  tval;
    //printf( "mbus %I64x:\n", addr );

    addr &= ~0x3;
    if ( (addr + 3) >= g_mbus_ctx.memsz )
    {
        return 1;
    }
    
    /**/
    tval = *(uint32_t *)(g_mbus_ctx.pmem + addr);
    *pval = utls_htonl(tval);
    return 0;
}


int  mbus_read_octa( uint64_t addr, uint64_t * pval )
{
    uint64_t  tval;

    addr &= ~0x7;
    if ( (addr + 7) >= g_mbus_ctx.memsz )
    {
        uint16_t  bid;

        /**/
        bid = addr >> 48;
        if ( bid >= MBUS_PERI_MAX )
        {
            return 11;
        }
        
        return peri_read_octa( bid, (addr & 0xfffffffffff8ull), pval );
    }
    
    /**/
    tval = *(uint64_t *)(g_mbus_ctx.pmem + addr);
    *pval = utls_htonll(tval);
    return 0;
}


int  mbus_write_byte( uint64_t addr, uint8_t val )
{
    if ( addr >= g_mbus_ctx.memsz )
    {
        return 1;
    }
    
    /**/
    *(g_mbus_ctx.pmem + addr) = val;
    return 0;
}


int  mbus_write_wyde( uint64_t addr, uint16_t val )
{
    addr &= ~0x1;
    if ( (addr + 1) >= g_mbus_ctx.memsz )
    {
        return 1;
    }
    
    /**/
    *(uint16_t *)(g_mbus_ctx.pmem + addr) = utls_htons(val);
    return 0;
}


int  mbus_write_tetra( uint64_t addr, uint32_t val )
{
    addr &= ~0x3;
    if ( (addr + 3) >= g_mbus_ctx.memsz )
    {
        return 1;
    }
    
    /**/
    *(uint32_t *)(g_mbus_ctx.pmem + addr) = utls_htonl(val);
    return 0;
}


int  mbus_write_octa( uint64_t addr, uint64_t val )
{
    addr &= ~0x7;
    if ( (addr + 7) >= g_mbus_ctx.memsz )
    {
        uint16_t  bid;

        /**/
        bid = addr >> 48;
        if ( bid >= MBUS_PERI_MAX )
        {
            return 11;
        }
        
        return peri_write_octa( bid, (addr & 0xfffffffffff8ull), val );
    }
    
    /**/
    *(uint64_t *)(g_mbus_ctx.pmem + addr) = utls_htonll(val);
    return 0;
}


/* read data block from mbus */
int  mbus_read_block( uint64_t addr, int tlen, void * pdat )
{
    uint64_t  temp;
    uint8_t * ptr;
    
    /**/
    temp = g_mbus_ctx.memsz;
    if ( addr >= temp )
    {
        return 1;
    }

    temp = addr + tlen;
    if ( temp < addr )
    {
        return 2;
    }

    if ( temp > g_mbus_ctx.memsz )
    {
        return 3;
    }

    /**/
    ptr = g_mbus_ctx.pmem + addr;
    memcpy( pdat, ptr, tlen );
    return 0;
    
}


/* write data block to mbus */
int  mbus_write_block( uint64_t addr, int tlen, void * pdat )
{
    uint64_t  temp;
    uint8_t * ptr;
    
    /**/
    temp = g_mbus_ctx.memsz;
    if ( addr >= temp )
    {
        return 1;
    }

    temp = addr + tlen;
    if ( temp < addr )
    {
        return 2;
    }

    if ( temp > g_mbus_ctx.memsz )
    {
        return 3;
    }

    /**/
    ptr = g_mbus_ctx.pmem + addr;
    memcpy( ptr, pdat, tlen );
    return 0;

}



int  mbus_register_peri( uint16_t bid, uint8_t irq, mbus_pread_func_t rfunc, mbus_pwrite_func_t wfunc, intptr_t arg )
{
    /**/
    if ( bid >= MBUS_PERI_MAX )
    {
        return 1;
    }

    /**/
    g_mbus_ctx.peri_info[bid].arg = arg;
    g_mbus_ctx.peri_info[bid].irq = irq;
    g_mbus_ctx.peri_info[bid].rfunc = rfunc;
    g_mbus_ctx.peri_info[bid].wfunc = wfunc;
    g_mbus_ctx.peri_info[bid].name = "dev";
    
    return 0;
}


/* mem size, bid num */
int  mbus_init( size_t memsz )
{
    int  i;
    mbus_context_t * pctx = &g_mbus_ctx;

    /**/
    pctx->pmem = (uint8_t *)malloc( memsz );
    if ( NULL == pctx->pmem )
    {
        return 0;
    }
    
    /**/
    pctx->memsz = memsz;
    
    /**/
    for ( i=0; i<MBUS_PERI_MAX; i++ )
    {
        pctx->peri_info[i].bid = 0xff;
        pctx->peri_info[i].irq = 0xff;

        pctx->peri_info[i].arg = 0;
        pctx->peri_info[i].rfunc = NULL;
        pctx->peri_info[i].wfunc = NULL;
    }
    
    return 0;
    
}


