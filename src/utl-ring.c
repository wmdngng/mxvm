
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "utl-ring.h"


int  u8_ring_init( u8_ring_t * rg, uint32_t count )
{
    rg->size = count;
    rg->mask = count - 1;
    rg->head = 0;
    rg->tail = 0;

    return 0;
    
}


