
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

/* already copy libuv to mingw dir */
#include <uv.h>

#include "mx-bus.h"
#include "mx-core.h"

#include "dev-tty.h"


/* read from file, write to mbus memory */
int  bootrom_from_file( char * fname, uint64_t addr )
{
    int  iret;
    int  temp;
    int  tsize;
    uint64_t  taddr;
    
    FILE * fp;
    uint8_t  tbuf[1024];
    
    /**/
    fp = fopen( fname, "rb" );
    if ( NULL == fp )
    {
        return 1;
    }

    /**/
    tsize = 0;
    taddr = addr;
    while(1)
    {
        temp = fread( tbuf, 1, 1024, fp );

        if ( temp <= 0 )
        {
            break;
        }
        
        iret = mbus_write_block( taddr, temp, tbuf );
        if ( 0 != iret )
        {
            return 2;
        }

        tsize += temp;
        taddr += temp;
        
        /**/
        if ( temp < 1024 )
        {
            break;
        }
    }

    /**/
    printf( "boot rom addr = 0x%x, size = 0x%x\n", (uint32_t)addr, tsize );
    return 0;
    
}



static void  timer_repeat_cb( uv_timer_t* handle) {
  return;
}

int  main( void )
{
    int  iret;
    uv_loop_t * ploop;
    uv_timer_t  ttimer;
    
	printf( "test..\n" );

    /* mbus and memory init, arg is mem size. */
    iret = mbus_init( 0x20000000 );
    if ( 0 != iret )
    {
        printf( "test bus init 512M fail, %d\n", iret );
        return 1;
    }

    /* wite memory boot content. */
    iret = bootrom_from_file( "bootrom.bin", 0 );
    if ( 0 != iret )
    {
        printf( "test bootrom file fail, %d\n", iret );
        return 2;
    }
    
    /* peripheral device init */
	ploop =  uv_default_loop();
    if ( NULL == ploop )
    {
        return 1;
    }

    iret = dev_tty_init( ploop, 1 );
    if ( 0 != iret )
    {
        return 56;
    }
    
    /* finally is core init. */
    iret = mcore_init( 0 );
    if ( 0 != iret )
    {
        printf( "test core init fail, %d\n", iret );
        return 11;
    }

  	iret = uv_timer_init( ploop, &ttimer);
  	iret = uv_timer_start( &ttimer, timer_repeat_cb, 1000, 1000 );
    uv_run( ploop, UV_RUN_DEFAULT );
		
		return 0;
	
}


