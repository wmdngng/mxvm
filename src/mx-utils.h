
#ifndef  __MX_UTILS_H__
#define  __MX_UTILS_H__

static inline uint16_t utls_htons( uint16_t val )
{
    return __builtin_bswap16(val);
}


static inline uint32_t utls_htonl( uint32_t val )
{
    return __builtin_bswap32(val);
}


static inline uint64_t utls_htonll( uint64_t val )
{
    return __builtin_bswap64(val);
}



#endif

