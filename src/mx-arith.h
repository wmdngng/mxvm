
#ifndef  __MX_ARITH_H__
#define  __MX_ARITH_H__

#define  AX_BIT (1)           // floating inexact
#define  AZ_BIT (1<<1)        // floating division by zero
#define  AU_BIT (1<<2)        // floating underflow
#define  AO_BIT (1<<3)        // floating overflow
#define  AI_BIT (1<<4)        // invalid operation
#define  AW_BIT (1<<5)        // float-to-fix overflow
#define  AV_BIT (1<<6)        // integer overflow
#define  AD_BIT (1<<7)        // integer divide check

#endif

