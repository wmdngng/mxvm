
#ifndef  __MX_BUS_H__
#define  __MX_BUS_H__

/*
core --> bus
    read: byte, wyde, tetra, octa
    write: byte, wyde, tetra, octa

return:
    0 - for success.
    1 - for memory not exist.

    // rQ, rK : machine field
    The rightmost bit stands for power failure, 
    the next for memory parity error, 
    the next for nonexistent memory, 
    the next for rebooting, etc.
*/

int  mbus_read_byte( uint64_t addr, uint8_t * pval );
int  mbus_read_wyde( uint64_t addr, uint16_t * pval );
int  mbus_read_tetra( uint64_t addr, uint32_t * pval );
int  mbus_read_octa( uint64_t addr, uint64_t * pval );

int  mbus_write_byte( uint64_t addr, uint8_t val );
int  mbus_write_wyde( uint64_t addr, uint16_t val );
int  mbus_write_tetra( uint64_t addr, uint32_t val );
int  mbus_write_octa( uint64_t addr, uint64_t val );

int  mbus_read_block( uint64_t addr, int tlen, void * pdat );
int  mbus_write_block( uint64_t addr, int tlen, void * pdat );


/* 
peri --> bus --> core 
request interrupt.
*/



/*
peri --> bus, register info :
    1, memory map io block id.
    2, interrupt number.
    3, read & write function, context arg.
*/

typedef int  (*mbus_pread_func_t)( intptr_t arg, uint64_t addr, uint64_t * pval );
typedef int  (*mbus_pwrite_func_t)( intptr_t arg, uint64_t addr, uint64_t val );

int  mbus_register_peri( uint16_t bid, uint8_t irq, mbus_pread_func_t rfunc, mbus_pwrite_func_t wfunc, intptr_t arg );

/* allow called by peri thread. */
int  mbus_report_irq( uint16_t bid, uint8_t irq );

/**/
int  mbus_copy_to_peri( uint64_t addr, void * pmem, int tlen );
int  mbus_copy_fr_peri( uint64_t addr, void * pmem, int tlen );


/**/
int  mbus_init( size_t memsz );


#endif


